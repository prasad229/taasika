<?php
require_once('./db.php');
error_log($_POST["SSname"]);
if($_FILES["zip_file"]["name"]) {
	$filename = $_FILES["zip_file"]["name"];
	$source = $_FILES["zip_file"]["tmp_name"];
	$type = $_FILES["zip_file"]["type"];
	
	$name = explode(".", $filename);
	$accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
	foreach($accepted_types as $mime_type) {
		if($mime_type == $type) {
			$okay = true;
			break;
		} 
	}
	
	$continue = strtolower($name[1]) == 'zip' ? true : false;
	if(!$continue) {
		$message = "The file you are trying to upload is not a .zip file. Please try again.";
	
	}
	//if(!file_exists("uploads")){
	    if(PHP_OS == 'Windows' || PHP_OS == 'WINNT' || PHP_OS == 'WIN32'){
	        if(file_exists(sys_get_temp_dir()."/uploads")){
	            mkdir(sys_get_temp_dir()."/uploads";
	        }
	    }
	//}
		
	$target_path = "uploads/".$filename;  // change this to the correct site path
	if(move_uploaded_file($source, $target_path)) {
		$zip = new ZipArchive();
		$x = $zip->open($target_path);
		if ($x === true) {
			$zip->extractTo("uploads/"); // change this to the correct site path
			$zip->close();
	
			unlink($target_path);
		}
		$message = "Your .zip file was uploaded and unpacked.";
		echo $message;
	}


//Actual code of importJSON starts here...	
	$tableNames = array(
		//Level  1 Tables(no foreign key)
		"room", "id", [ "roomName","roomShortName", "roomCount"], 
		"subject", "id", ["subjectName","subjectShortName",  "eachSlot", "nSlots", "batches"],
		"class", "id", ["className", "classShortName","semester", "classCount"],
		"batch","id", ["batchName", "batchCount"],
		"teacherReadable", "name",
		["teacherName", "teacherShortName", "minHrs", "maxHrs", "deptShortName"],
		//Level 2 Tables(2 foreign keys)
		"batchClassReadable", "name", ["batchName", "classShortName"], 
		"batchRoomReadable", "name", ["batchName", "roomShortName"],
		"classRoomReadable", "name", ["classShortName", "roomShortName"],
		"subjectRoomReadable", "name", ["subjectShortName", "roomShortName"],
		"batchCanOverlapReadable", "name",
			["b1Name", "b2Name"],
		//Level 3 Tables (3 foreign keys)
		"subjectClassTeacherReadable", "name",
			["classShortName", "subjectShortName", "teacherShortName"],
		"subjectBatchTeacherReadable", "name",
			["batchName", "subjectShortName", "teacherShortName"],
		"overlappingSBTReadable", "name", ["subject1", "batch1", "teacher1", "subject2", "batch2", "teacher2"],
		
		"config", "none",["configName", "dayBegin", "slotDuration", "nSlots", "incharge"],
	
		"timeTableReadable", "name", [ "day", "slotNo", "roomShortName", "classShortName", "subjectShortName", "teacherShortName", "batchName", "isFixed", "snapshotName"]	
	);
	//Code to check if all the needed files are uploaded and extracted; otherwise abort the operation...
	$allFilesExtracted = true;
	$neededFiles = "Following Files are missing in the zip or not extracted successfully!<br>";
	$folderName = rtrim($filename, ".zip");
	for($x = 0; $x < count($tableNames); $x += 3){
		error_log("uploads/".$folderName."/".$tableNames[$x].".JSON");
		if(!file_exists("uploads/".$folderName."/".$tableNames[$x].".JSON")){
			$allFilesExtracted = false;
			$neededFiles = $neededFiles.$tableNames[$x]."<br>";
		}
	}

	if($allFilesExtracted == true){
	$maxSS = findMaxId(); //finding the current max snapshotid 
	$SS = $maxSS + 1;
	$SSName = $_POST["SSname"];	
	//$configData = file_get_contents ("tmp/timetable/$config.JSON");
	$qs = 'insert into snapshot values('.$SS.', "'.$SSName.'", 1, "08:00:00", "08:00:00", 1);'; //inserting new ssid for importdata
		//ssname given by default as snap+ssid
	error_log($qs);
	insertQuery($qs);		
	//error_log(var_dump($maxSS));
	
	
//Inserting data into level-1 tables...timetable.php
	for($i = 0; $i < 15; $i += 3) {
		$vals = "";
		$currTableName = $tableNames[$i];		
		$data = file_get_contents ("uploads/".$folderName."/".$currTableName.".JSON");		
        	$json = json_decode($data, true);
		$n = count($json);
		$nattr = count($tableNames[$i+2]); 		
		for($j = 0; $j < $n; $j++){
        		$vals = "NULL,";
			
			//$vals = "'".$json[$j][$tableNames[$i+2][0]]."',";
			for($k = 0; $k < $nattr; $k++){
				if($tableNames[$i+2][$k] == "deptShortName"){
					$vals = $vals."1,";
				}
				elseif(strlen($json[$j][$tableNames[$i+2][$k]]) == 0){
					$vals = $vals."NULL,";
					//error_log("Inside The NULL generator");
				}
				else{				
					$vals = $vals."'".$json[$j][$tableNames[$i+2][$k]]."',";
				}			
			}	
			//$vals = rtrim($vals, ",");
			if(strpos($tableNames[$i],'Readable' )!== false){
					$currTableName = "teacher";
				}
			$vals = $vals.$SS;
			$q = "insert into ".$currTableName." values(".$vals.");"; 
			//error_log($q); 
			insertQuery($q);				
		}
		//insertQuery("insert into fixedEntry values('2', '4', 'LUNCH', '1');");
        }
	
//Inserting data into level-2 tables...	
	for($i = 15; $i < 34; $i += 3) {
		$vals = "";
		$currTableName = $tableNames[$i];		
		$data = file_get_contents ("uploads/".$folderName."/".$currTableName.".JSON");		
		
		//var_dump($i);
		//var_dump($currTableName);			
			        	
		$json = json_decode($data, true);
		$n = count($json);
		$nattr = count($tableNames[$i+2]); 
		for($j = 0; $j < $n; $j++){
        		$vals = "NULL,";
			for($k = 0; $k < $nattr; $k++){
				$name = $json[$j][$tableNames[$i+2][$k]];
				$dbTable = rtrim($tableNames[$i+2][$k], "Name");
				if(($currTableName == "subjectBatchTeacherReadable")||($currTableName == "subjectClassTeacherReadable")){
					if(($dbTable == "classShort")||($dbTable == "batch")){
						$name = $json[$j][$tableNames[$i+2][1]];						
						$tempId = getIdByName($name, "subjectShort", $SS);					
						}
					elseif(($dbTable == "subjectShort")&&($currTableName == "subjectBatchTeacherReadable")){
						$name = $json[$j][$tableNames[$i+2][0]];						
						$tempId = getIdByName($name, "batch", $SS);						}
					elseif(($dbTable == "subjectShort")&&($currTableName == "subjectClassTeacherReadable")){
						$name = $json[$j][$tableNames[$i+2][0]];						
						$tempId = getIdByName($name, "classShort", $SS);						
						}
					else
						$tempId = getIdByName($name, $dbTable, $SS);						
					}
				else{ 
					$tempId = getIdByName($name, $dbTable, $SS);
					}				
			$vals = $vals.$tempId." ,";
			
			}
			$vals = $vals.$SS;
			$insTable = rtrim($currTableName, "Readable");
			$q = "insert into ".$insTable." values(".$vals.");"; 
			//error_log($q);
			insertQuery($q);
		}
	}
	
	//For overlappingSBT TAble only...
	$i = 36;
	$currTableName = $tableNames[$i];		
	$data = file_get_contents ("uploads/".$folderName."/".$currTableName.".JSON");		
	$json = json_decode($data, true);
	$n = count($json);
	$nattr = count($tableNames[$i+2]); 
	for($j = 0; $j < $n; $j++){
		$sbtArray = array();
		for($k= 0; $k < 2; $k++){
			$subject = $json[$j][$tableNames[$i+2][0+$k*3]];
			$batch = $json[$j][$tableNames[$i+2][1+$k*3]];
			$teacher = $json[$j][$tableNames[$i+2][2+$k*3]];
			$subjectId = getIdByName($subject, "subjectShort", $SS);
			$batchId = getIdByName($batch, "batch", $SS);
			$teacherId = getIdByName($teacher, "teacherShort", $SS);
			//$vals = "NULL";
			$q = "select sbtId from subjectBatchTeacher where subjectId = ".$subjectId." and batchId = ".$batchId." and teacherId = ".$teacherId." ; ";
			$toFind = "sbtId";
			$tmpId = insertQueryResult($q, $toFind);
			$sbtArray[$k] = $tmpId;
		}
		var_dump($sbtArray);
		$q = "insert into overlappingSBT values ( NULL, ".$sbtArray[0].", ".$sbtArray[1].", ".$SS." );";
		var_dump($q);		
		insertQuery($q);	
	}
	

	//fOR TIMETABLE Table only...
		$i = 42;
		$vals = "";
		$currTableName = $tableNames[$i];		
		$data = file_get_contents ("uploads/".$folderName."/".$currTableName.".JSON");		
        	$json = json_decode($data, true);
		$n = count($json);
		$nattr = count($tableNames[$i+2]); 
		for($j = 0; $j < $n; $j++){
        		$vals = "NULL, ";
			$vals = $vals.$json[$j][$tableNames[$i+2][0]].", ";
			$vals = $vals.$json[$j][$tableNames[$i+2][1]].", ";
			for($k = 2; $k < 7; $k++){
				$name = $json[$j][$tableNames[$i+2][$k]];
				if($name == ""){
					$vals = $vals."NULL, ";		
				}
				else{
					$dbTable = rtrim($tableNames[$i+2][$k], "Name");
					$tempId = getIdByName($name, $dbTable, $SS);
					$vals = $vals.$tempId." ,";
				}			
			}
			$vals = $vals.$json[$j][$tableNames[$i+2][7]].", ";
			$vals = $vals.$SS;
			$insTable = rtrim($currTableName, "Readable");
			if($insTable != "timeTable"){
				$insTable = "timeTable";
			}
			$q = "insert into ".$insTable." values(".$vals.");"; 
			var_dump($q);
			insertQuery($q);
		}
	//For fixedEntry Table Only...
		$vals = "";
		$currTableName = "fixedEntry";		
		$q = "select ttId from timeTable where isFixed = 1 and snapshotId = ".$SS.";";
		$result = getResultArray($q);
		$res = array();
		$res = $result->fetch_all(MYSQLI_ASSOC);
		$n = count($res);
		for($i = 0; $i < $n; $i++){ 
			$ttId = $res[$i]["ttId"];
			var_dump($ttId);
			//$res = ltrim($res, '"');
			//$res = rtrim($res, '"');
			$q = "insert into fixedEntry values (NULL, ".$ttId.", 'LUNCH', ".$SS.");";
			var_dump($q);
			insertQuery($q);
	 	}
		//Now after successful import we redirect to timetable.php
		echo "<script>location.href = 'timetable.php';</script>";

	} 
	if($allFilesExtracted != true) {	//$allFilesExtracted == false
		$message = "There was a problem with the upload. Please try again. Please check the path of uploads/timetable in up.php.<br>";
		$message = $message.$neededFiles;
		echo $message;	
	}
}

?>
