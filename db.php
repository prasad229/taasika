<?php
// This file is part of Taasika - a timetabling software for 
// schools, colleges/universities.
//
// Taasika is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Taasika is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Taasika.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * Copyright 2017 Abhijit A. M.(abhijit13@gmail.com)
 */
require_once('config.php');
require_once('common.php');
function dbConnect() {
	global $CFG;
	if ($CFG->conn != false) {
		return $CFG->conn;
	}
	$conn = new mysqli($CFG->server, $CFG->db_user, $CFG->db_pass, $CFG->db_database);
	if($conn->connect_error) {
		die("connection error ". $conn->connect_error . "<br>");
		return false;
	}
	$CFG->conn = $conn;
	return $conn;
}
function dbConnectNoDatabase() {
	global $CFG;
	if ($CFG->conn != false) {
		return $CFG->conn;
	}
	$conn = new mysqli($CFG->server, $CFG->db_user, $CFG->db_pass);
	if($conn->connect_error) {
		return false;
	}
	$CFG->conn = $conn;
	return $conn;
}
function findMaxId(){
	global $CFG;
	if($CFG->conn === false) 
		$conn = dbConnect();
	else
		$conn = $CFG->conn;
	if($conn === false) {
		die("db.php: Database connection error");
	}
	$result = $conn->query("select max(snapshotId) from snapshot;");
	$max = array();
	$max = $result->fetch_all(MYSQLI_ASSOC);
	$no = $max[0]["max(snapshotId)"];
	$no = rtrim($no, '"');
	$no = ltrim($no, '"');
	$no = (int)$no;	
	//error_log(var_dump($max[0]["max(snapshotId)"]));
	//error_log(gettype($no));
		
	return $no;
}
function createDatabase($dbName) {
	$conn = dbConnectNoDatabase();
	$sqlQuery = "CREATE DATABASE ".$dbName.";";
	$result = $conn->query($sqlQuery);
	if($result === false)
		return false;
	return $result;
}
function insertQuery($q){
	global $CFG;
	if($CFG->conn === false) 
		$conn = dbConnect();
	else
		$conn = $CFG->conn;
	if($conn === false) {
		die("db.php: Database connection error");
	}
	$result = $conn->query($q);
}
function sqlGetAllRows($query) {
	global $CFG;
	if($CFG->conn === false) 
		$conn = dbConnect();
	else
		$conn = $CFG->conn;
	if($conn === false) {
		die("db.php: Database connection error");
	}
	$result = $conn->query($query);
	if($result === false)  {
		ttlog("sqlGetAllRows: Query $query returned false");
		$CFG->last_query = $query;
		die("Query $query returned false");
	}
	$allrows = array();
	$allrows = $result->fetch_all(MYSQLI_ASSOC);
	return $allrows;
}
function sqlGetOneRow($query) {
	global $CFG;
	if($CFG->conn === false) 
		$conn = dbConnect();
	else
		$conn = $CFG->conn;
	if($conn === false) {
		die("db.php: Database connection error");
	}
	$result = $conn->query($query);
	if($result === false)  {
		ttlog("sqlGetOneRow: $query Failed");
		die("sqlGetOneRow: Query $query returned false");
	}
	$allrows = array();
	$allrows = $result->fetch_all(MYSQLI_ASSOC);
	if(count($allrows) != 1) {
		ttlog("sqlGetOneRow: $query returned ".count($allrows)." rows");
		$CFG->last_query = $query;
		die("sqlGetOneRow: $query returned ".count($allrows)." rows");
	}
	return $allrows;
}

function sqlUpdate($query) {
	global $CFG;
	if($CFG->conn === false)
		$conn = dbConnect();
	else
		$conn = $CFG->conn;
	if($conn === false) {
		die("db.php: Database connection error");
	}
	$result = $conn->query($query);
	if($result === false) {
		$CFG->last_query = $query;
		ttlog("sqlUpdate: Query $query returned false\n");	
		return $result;
	}
	return true;
}
function getIdByName($name, $dbTable, $SS){
	global $CFG;
	if($CFG->conn === false) 
		$conn = dbConnect();
	else
		$conn = $CFG->conn;
	if($conn === false) {
		die("db.php: Database connection error");
	}
	
	$t = rtrim($dbTable, "Short");
	var_dump($dbTable);
	if($t == "teache"){
		$t = "teacher";		
		}	
		
	if(($t == "batc")||($t == "b1" )|| ($t == "b2")){
		$t = "batch";
			
	}
	if($t == "subjec"){
		$t = "subject";
	}
	if($dbTable == "batchNA"){
		$dbTable = "batch";
	}
	
	if($t == "batchNA"){
		$t = "batch";
	}
	if(($dbTable == "b1")||($dbTable == "b2")){
		$dbTable = "batch";		
		}
		$query = "select ".$t."Id from ".$t." where ".$dbTable."Name = '".$name."' and snapshotId = $SS;";
	var_dump($query);
	$result = $conn->query($query);
	$res = array();
	$res = $result->fetch_all(MYSQLI_ASSOC);
	$res = $res[0][$t."Id"];
	$res = ltrim($res, '"');
	$res = rtrim($res, '"');
	var_dump($res);
	return $res;
	//error_log($result[$dbTable."Id"]);
}
function insertQueryResult($q, $toFind){
	global $CFG;
	if($CFG->conn === false) 
		$conn = dbConnect();
	else
		$conn = $CFG->conn;
	if($conn === false) {
		die("db.php: Database connection error");
	}
	$result = $conn->query($q);
	$res = array();
	$res = $result->fetch_all(MYSQLI_ASSOC);
	$res = $res[0][$toFind];
	var_dump($res);
	return $res;	
}
function getResultArray($q){
	global $CFG;
	if($CFG->conn === false) 
		$conn = dbConnect();
	else
		$conn = $CFG->conn;
	if($conn === false) {
		die("db.php: Database connection error");
	}
	$result = $conn->query($q);
	var_dump($q);
	var_dump($result);
	return $result;
}
?>
